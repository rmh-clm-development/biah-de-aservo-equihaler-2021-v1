module.exports = {

  start: 'core-010-000',
  summary: null,

  map: {},

  stories: {
    core: {
      'core-010-000': {
        left: null,
        right: 'core-020-000',
      },
      'core-020-000': {
        left: 'core-010-000',
        right: 'core-030-000',
        down: 'core-020-010',
      },
      'core-020-010': {
        left: 'core-010-000',
        right: 'core-030-000',
        up: 'core-020-000',
      },
      'core-030-000': {
        left: 'core-020-000',
        right: 'core-040-000',
      },
      'core-040-000': {
        left: 'core-030-000',
        right: 'core-070-000',
        down: 'core-050-000'
      },
      'core-050-000': {
        left: 'core-030-000',
        right: 'core-070-000',
        up: 'core-040-000',
        down: 'core-060-000'
      },
      'core-060-000': {
        left: 'core-030-000',
        right: 'core-070-000',
        up: 'core-050-000'
      },
      'core-070-000': {
        left: 'core-040-000',
        right: 'core-080-000',
      },
      'core-080-000': {
        left: 'core-070-000'
      },
    },

    extra: {
      'extra-010-000': {},
      'extra-020-000': {},
      'extra-030-000': {},
      'extra-040-000': {},
      'extra-050-000': {},
      'extra-060-000': {
        down: 'extra-070-000'
      },
      'extra-070-000': {
        up: 'extra-060-000'
      }
    }
  },

  layers: [
    {
      'id': 'story',
      'type': 'STORY',

      'story': 'core',
      'slide': 'core-010-000',

      'dimensions': {
        'width': 1024,
        'height': 768
      },

      'position': {
        'x': 0,
        'y': 0
      }
    }
  ]
}