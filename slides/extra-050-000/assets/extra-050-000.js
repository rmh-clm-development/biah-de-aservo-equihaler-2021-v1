var cmd = eda.app.command('events.addListener', {
  event: 'slideShown',
  slideId: 'extra-050-000'
})

cmd.on('eventOccurred', function() {
  $$('.toggler--one').on('tap', function() {
    $$('.part--one').toggleClass('show')
  })

  $$('.toggler--two').on('tap', function() {
    $$('.part--two').toggleClass('show')
  })

  $$('.toggler--three').on('tap', function() {
    $$('.part--three').toggleClass('show')
  })
})

cmd.send()