var cmd = eda.app.command('events.addListener', {
  event: 'slideInitialized',
  slideId: 'core-030-000'
})

let toggler = true

cmd.on('eventOccurred', function () {

  setTimeout(function () {
    $$('.arrow').addClass('show')
  }, 800)

  $$('.eye-active').on('tap', function () {
    if (toggler) {
      $$('.arrow--one').toggleClass('hide')
      $$('.arrow--two').toggleClass('hide')
      $$('.arrow--three').toggleClass('hide')
      $$('.arrow').addClass('hide')

      $$('.horse-second').toggleClass('show')
      toggler = false
    }
    else {
      $$('.horse-second').toggleClass('show')

      $$('.arrow--one').toggleClass('hide')
      $$('.arrow--two').toggleClass('hide')
      $$('.arrow--three').toggleClass('hide')
      $$('.arrow').removeClass('hide')

      toggler = true
    }
  })
})

cmd.send()