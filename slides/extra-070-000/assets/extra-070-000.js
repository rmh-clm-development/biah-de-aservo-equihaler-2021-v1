var cmd = eda.app.command('events.addListener', {
  event: 'slideShown',
  slideId: 'extra-070-000'
})

cmd.on('eventOccurred', function() {
  $$('.toggler').on('tap', function () {
    $$('.popup-content').toggleClass('show')
    $$('.toggler').toggleClass('show')
  })
})
cmd.send()