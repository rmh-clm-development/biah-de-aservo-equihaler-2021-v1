var cmd = eda.app.command('events.addListener', {
  event: 'slideShown',
  slideId: 'extra-060-000'
})

let contentImgs = [
  '.content--1',
  '.content--2'
]

cmd.on('eventOccurred', function() {
  $$('.toggler').on('tap', function () {
    contentImgs.forEach(img => {
      if ($$(img).hasClass('show')) {
        $$(img).toggleClass('show')
      }
      else {
        $$(img).toggleClass('show')
      }
    })
  })
})
cmd.send()