var cmd = eda.app.command('events.addListener', {
  event: 'slideInitialized',
  slideId: 'core-040-000'
})

let popups = [
  '.popup--1',
  '.popup--2',
  '.popup--3',
  '.popup--4'
]

cmd.on('eventOccurred', function () {

  $$('#aktivierung').on('tap', function() {
    togglePopup(popups[0])
  })
  $$('#auslosen').on('tap', function() {
    togglePopup(popups[1])
  })
  $$('#nusternadapter').on('tap', function() {
    togglePopup(popups[2])
  })
  $$('#lagerung').on('tap', function() {
    togglePopup(popups[3])
  })


  $$('.popup--1').on('swipeLeft', function() {
    $$(this).toggleClass('show')
    $$('.popup--2').toggleClass('show')
  })

  $$('.popup--2').on('swipeLeft', function() {
    $$(this).toggleClass('show')
    $$('.popup--3').toggleClass('show')
  })
  $$('.popup--2').on('swipeRight', function() {
    $$(this).toggleClass('show')
    $$('.popup--1').toggleClass('show')
  })

  $$('.popup--3').on('swipeLeft', function() {
    $$(this).toggleClass('show')
    $$('.popup--4').toggleClass('show')
  })
  $$('.popup--3').on('swipeRight', function() {
    $$(this).toggleClass('show')
    $$('.popup--2').toggleClass('show')
  })

  $$('.popup--4').on('swipeRight', function() {
    $$(this).toggleClass('show')
    $$('.popup--3').toggleClass('show')
  })

  // Video
  $$('.video-btn').on('tap', function () {
    $$('.slide-background').addClass('hide')
    $$('.video video')[0].play()
    $$('.video').addClass('isPlaying')
    $$('video')[0].webkitEnterFullScreen()
  })

  $$('.widget.video:not(.isPlaying)').on('tap', function () {
    $$('video')[0].webkitEnterFullScreen()
  })

  $$('video')[0].addEventListener('ended', function () {
    $$(this)[0].webkitExitFullscreen()
    $$('.slide-background').removeClass('hide')
    $$('.video').removeClass('isPlaying')
  })

  //check for fullscreen change event
  $$(document).on('webkitfullscreenchange', function () {
    checkFullscreen()
  })
})

function checkFullscreen () {
  let fullscreen = document.webkitIsFullScreen
  if (!fullscreen) {
    $$('.widget.video video')[0].pause()
    $$('.widget.video').removeClass('isPlaying')
  }
}

function togglePopup(id) {
  if ($$(id).hasClass('show')) {
    return
  }

  $$(id).toggleClass('show')
  popups.forEach(popup => {
    if ($$(popup).hasClass('show') && popup != id) {
      $$(popup).toggleClass('show')
    }
  })
}

cmd.send()