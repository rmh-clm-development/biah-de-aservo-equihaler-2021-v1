var cmd = eda.app.command('events.addListener', {
  event: 'slideInitialized',
  slideId: 'core-060-000'
})

let btnClicked = false

cmd.on('eventOccurred', function () {

   // Video
  $$('.video-btn').on('tap', function () {
    $$('.slide-background').addClass('hide')
    $$('.video video')[0].play()
    $$('.video').addClass('isPlaying')
    $$('video')[0].webkitEnterFullScreen()
  })

  $$('.widget.video:not(.isPlaying)').on('tap', function () {
    $$('video')[0].webkitEnterFullScreen()
  })

  $$('video')[0].addEventListener('ended', function () {
    $$(this)[0].webkitExitFullscreen()
    $$('.slide-background').removeClass('hide')
    $$('.video').removeClass('isPlaying')
  })

  //check for fullscreen change event
  $$(document).on('webkitfullscreenchange', function () {
    checkFullscreen()
  })
})

cmd.send()

function checkFullscreen () {
  let fullscreen = document.webkitIsFullScreen
  if (!fullscreen) {
    $$('.widget.video video')[0].pause()
    $$('.widget.video').removeClass('isPlaying')
  }
}