var cmd = eda.app.command('events.addListener', {
  event: 'slideInitialized',
  slideId: 'core-050-000'
})

$$('.popup').css('display', 'block')

cmd.on('eventOccurred', function () {

  $$('.popup--one img:nth-child(2)').on('tap', function() {
    $$('.popup--one').toggleClass('show')

    if ($$('.popup--two').hasClass('show')) {
      $$('.popup--two').toggleClass('show')
    }
  })

  $$('.popup--two img:nth-child(2)').on('tap', function() {
    $$('.popup--two').toggleClass('show')

    if ($$('.popup--one').hasClass('show')) {
      $$('.popup--one').toggleClass('show')
    }
  })


   // Video
  $$('.video-btn').on('tap', function () {
    $$('.slide-background').addClass('hide')
    $$('.video video')[0].play()
    $$('.video').addClass('isPlaying')
    $$('video')[0].webkitEnterFullScreen()
  })

  $$('.widget.video:not(.isPlaying)').on('tap', function () {
    $$('video')[0].webkitEnterFullScreen()
  })

  $$('video')[0].addEventListener('ended', function () {
    $$(this)[0].webkitExitFullscreen()
    $$('.slide-background').removeClass('hide')
    $$('.video').removeClass('isPlaying')
  })

  //check for fullscreen change event
  $$(document).on('webkitfullscreenchange', function () {
    checkFullscreen()
  })
})


function checkFullscreen () {
  let fullscreen = document.webkitIsFullScreen
  if (!fullscreen) {
    $$('.widget.video video')[0].pause()
    $$('.widget.video').removeClass('isPlaying')
  }
}

cmd.send()