module.exports = {
    "slides": {},
    "documents": {},
    "veevaZipnames": {
        "slides": {
            "core-010-000": "DE_AH_Aservo_EquiHaler_2021_V1-core-010-000.zip",
            "core-020-000": "DE_AH_Aservo_EquiHaler_2021_V1-core-020-000.zip",
            "core-020-010": "DE_AH_Aservo_EquiHaler_2021_V1-core-020-010.zip",
            "core-030-000": "DE_AH_Aservo_EquiHaler_2021_V1-core-030-000.zip",
            "core-040-000": "DE_AH_Aservo_EquiHaler_2021_V1-core-040-000.zip",
            "core-050-000": "DE_AH_Aservo_EquiHaler_2021_V1-core-050-000.zip",
            "core-060-000": "DE_AH_Aservo_EquiHaler_2021_V1-core-060-000.zip",
            "core-070-000": "DE_AH_Aservo_EquiHaler_2021_V1-core-070-000.zip",
            "core-080-000": "DE_AH_Aservo_EquiHaler_2021_V1-core-080-000.zip",
            "extra-010-000": "DE_AH_Aservo_EquiHaler_2021_V1-extra-010-000.zip",
            "extra-020-000": "DE_AH_Aservo_EquiHaler_2021_V1-extra-020-000.zip",
            "extra-030-000": "DE_AH_Aservo_EquiHaler_2021_V1-extra-030-000.zip",
            "extra-040-000": "DE_AH_Aservo_EquiHaler_2021_V1-extra-040-000.zip",
            "extra-050-000": "DE_AH_Aservo_EquiHaler_2021_V1-extra-050-000.zip",
            "extra-060-000": "DE_AH_Aservo_EquiHaler_2021_V1-extra-060-000.zip",
            "extra-070-000": "DE_AH_Aservo_EquiHaler_2021_V1-extra-070-000.zip"
        },
        "documents": {}
    }
};