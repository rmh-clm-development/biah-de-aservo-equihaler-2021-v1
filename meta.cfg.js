module.exports = {

  // Email template
  'ET-0706': {
    type: 'emailTemplate',
    vaultData: {
      vaultDocument: '47511'
    }
  },

  //extra-060, 070
  'EF-0918': {
    type: 'emailFragment',
    vaultData: {
      vaultDocument: '48436'
    }
  },

  //core-040 popup1
  'EF-0919': {
    type: 'emailFragment',
    vaultData: {
      vaultDocument: '48437'
    }
  },

  //core-060 popup1
  'EF-0933': {
    type: 'emailFragment',
    vaultData: {
      vaultDocument: '48572'
    }
  }
}