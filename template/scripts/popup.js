$$(function () {

  if (!window.eda.currentSlide) return

  // popup close button (need for click effect)
  $$('.widget.modal .close').on('tap', function () {
    $$(this)
      .addClass('tapped')
      .on('animationend', tapEnd)
  })

  $$('.widget.toucharea').on('tap', function () {

    let touchAreaId = $$(this)[0].id
    let modalContainer = document.getElementById('modals')
    let childLength = modalContainer.children.length

    if (!touchAreaId.includes('popup') && childLength <= 1) {
      return
    }

    let referencePopup = document.getElementById('referenceList')
    referencePopup.parentNode.appendChild(referencePopup)
  })

  function tapEnd () {
    $$(this).removeClass('tapped')
    window.core.register[$$(this).closest('.widget.modal').data('register')].toggle()
  }
})