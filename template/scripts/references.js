$$(function () {
  if (!window.eda.currentSlide) {
    return
  }

  $$('[data-target="referenceList"]').on('tap', function () {
    createScroll()
  })
})


function createScroll () {
  setTimeout(function () {
    referencesScroll = new IScroll('#references-wrapper', {
      scrollbars: true,
      bounce: false
    })
  }, 150)
}