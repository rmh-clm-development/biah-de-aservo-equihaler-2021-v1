var cmd = eda.app.command('events.addListener', {
  event: 'slideInitialized',
  listener: 'set_active_slide_in_sitemap'
})

cmd.on('eventOccurred', function () {
  let myClickId = null

  window.eda.storage.get('story')
  .then((result) => {
    switch (result) {
      case 'core':
        setCoreSitemap()
      break;

      case 'extra':
        setExtraSitemap()
      break;
    }
  })

  const triggerClickId = () => {
    if (myClickId != null && myClickId != 'null') {
      $$(myClickId).trigger('tap')
      window.eda.storage.set('clickId', null)
    }

    setSitemapActiveState()
  }

  const setSitemapActiveState = () => {
    $$("#sitemap-popup [data-to=" + eda.currentSlide + "]").addClass('active')
    $$("#sitemap-popup .popup").removeClass('active')
  }

  window.eda.storage.get('clickId').then(clickId => {
    myClickId = clickId
    setTimeout(triggerClickId, 100)
  })

  $$('#sitemap-popup img[data-to]').each(function () {
    $$(this).on('contextmenu', function (e) {
      e.preventDefault()
      return false
    })
  })
})

cmd.send()

$$(function () {
  if (!window.eda.currentSlide) {
    return
  }

  $$("[data-toggle]").on('tap', function (e) {
    const action = e.target.dataset.toggle

    if (action != 'extras') {
      return
    }

    setExtraSitemap()

    $$('#close-sitemap').on('tap', function () {
      window.eda.storage.get('story').then((story) => {
        setTimeout(function () {
          if (story == 'core') {
            setCoreSitemap()
          }
          else {
            setExtraSitemap()
          }
        }, 250)
      })
    })
  })
})

function setExtraSitemap () {
  $$('.sitemap-background .core').addClass('hidden')
  $$('#sitemap-popup .core').addClass('hidden')

  $$('.sitemap-background .extra').removeClass('hidden')
  $$('#sitemap-popup .extra').removeClass('hidden')
}

function setCoreSitemap () {
  $$('.sitemap-background .core').removeClass('hidden')
  $$('#sitemap-popup .core').removeClass('hidden')

  $$('.sitemap-background .extra').addClass('hidden')
  $$('#sitemap-popup .extra').addClass('hidden')
}