$$(function () {

  if (!window.eda.currentSlide) return

  var touchTimer = undefined
  var longTap = false

  function tapEnd (e) {
    $$(this).removeClass('tapped')
    if($$(e.target).data('click') != null && $$(e.target).data('click') != 'null' && $$(e.target).data('click')) {
      window.eda.storage.set('clickId', $$(e.target).data('click'))
    }

    // save current slide in storage
    window.eda.storage.get('story')
    .then((story) => {
      if (story == 'extra') {
        return
      }
      window.eda.storage.set('previousCore', window.eda.currentSlide)
    })

    window.core.navigateTo($$(this).data('to'))
  }

  $$('#sitemap-popup img[data-to]').on('touchstart', function (e) {
    $$('.zoom')
      .removeClass('zoom')
      .css('z-index', 1)

    // prevent the navigateTo function
    e.stopImmediatePropagation()
    e.preventDefault()

    // start a timer so we can see if we're holding the tap or not
    touchTimer = setTimeout (function () {
      $$(e.target)
        .addClass('zoom')
        .css('z-index', 2)
      longTap = true
    }, 150)
  })

  $$('#sitemap-popup img[data-to]').on('touchend', function () {
    // if it was not a long tap navigate to slide
    if (longTap == false) {
      $$(this)
        .addClass('tapped')
        .on('animationend', tapEnd)
    }

    longTap = false
    clearTimeout(touchTimer)
  })

  $$('#sitemap-popup').on('tap', function () {
    clearTimeout(touchTimer)
    $$('#sitemap-popup img[data-to]')
      .removeClass('zoom')
      .css('z-index', 1)
  })
});
