getFlow = function() {
  return window.eda.currentSlide.split("-")[0]
}
let referencesScroll = null

$$(function () {
  if (!window.eda.currentSlide) return
  let flow = getFlow();
  $$(".veemailFragmentCount").addClass(flow); // color scheme switch by flow for VeemailCounter in Footer
  $$(".veemail-collect").addClass(flow); // color scheme switch by flow for Veemail envelop

  $$("[data-toggle]").on('tap', function (e) {
    const action = e.target.dataset.toggle
    switch (action) {
      // Top navigation
      case 'home':
        core.navigateTo( "core-010-000")
      break;
      case 'sitemap':
        window.core.register.sitemap.open()
      break;
      case 'pi':
        window.core.register.productinformation.open()
      break;
      // sitemap-helper.js search for logic
      case 'extras':
        window.core.register.sitemap.open()
      break;
      case 'fazit':
        core.navigateTo('core-080-000')
      break;
      // Footer
      case 'zuruck':
        switchStory()
      break;
      case 'references':
        window.core.register.referenceList.open()
        createScroll()
      break;
    }
  })

  function switchStory () {
    window.eda.storage.get('story')
    .then((result) => {
      if (result == 'core') {
        return
      }

      window.eda.storage.get('previousCore')
      .then((slide) => {
        core.navigateTo(slide)
      })
    })
  }

  $$("[data-to]").on('tap', function (e) {
    if (e.target.dataset.retard) {
      setTimeout(function () {
        window.core.navigateTo(e.target.dataset.to)
      }, 500)
    } else {
      window.core.navigateTo(e.target.dataset.to)
    }
    if (e.target.dataset.wobble) {
      e.target.classList.add('wobbled')
    }
  })

  core.register.sitemap.on('open', function () {
    hideNavigation()
  })
  core.register.referenceList.on('open', function () {
    hideNavigation()
  })
  core.register.productinformation.on('open', function () {
    hideNavigation()
  })


  core.register.sitemap.on('close', function () {
    showNavigation()
  })
  core.register.referenceList.on('close', function () {
    showNavigation()
  })
  core.register.productinformation.on('close', function () {
    showNavigation()
  })


  function hideNavigation() {
    $$("#menu,#footer").addClass('hidden')
  }

  function showNavigation() {
    $$("#menu,#footer").removeClass('hidden')
  }

  $$("#menu").addClass(window.eda.currentSlide.split("-")[0])
});
