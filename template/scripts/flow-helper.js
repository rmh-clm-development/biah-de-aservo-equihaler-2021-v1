var cmd = eda.app.command('events.addListener', {
  event: 'slideInitialized'
})

cmd.on('eventOccurred', function () {
  let slideId = window.eda.currentSlide

  if (slideId.includes('core')) {
    window.eda.storage.set('story', 'core')
    return
  }

  window.eda.storage.set('story', 'extra')
})

cmd.send()