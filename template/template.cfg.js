module.exports = {
  'assets': [
    'vendor/iscrolljs/iscroll.js',
    'less/layout.less',
    'scripts/popup.js',
    'scripts/zoom.js',
    'scripts/menu.js',
    'scripts/references.js',
    'scripts/flow-helper.js',
    'scripts/sitemap-helper.js'
  ]
}

