var name = 'DE AH Aservo EquiHaler 2021 V1';
var id = 'DE_AH_Aservo_EquiHaler_2021_V1';

var slideDisabledActions = 'Swipe, Pinch to Exit';
var presHidden = 'No';
var country = 'Germany';
var language = 'German';
var crmProductId = '0CB00000000Z001';

var msccvStage = {
  presentation: {
    'pres.product__v.name__v': '',
    'pres.country__v.name__v': country,
    'pres.language__v': language,
    'external_id__v': id,
    'pres.crm_hidden__v': presHidden,
    'pres.crm_presentation_id__v': id,
    'pres.crm_product__v.id': crmProductId,
  },
  slide: {
    'slide.product__v.name__v': '',
    'slide.language__v': language,
    'slide.country__v.name__v': country,
    'slide.crm_disable_actions__v': slideDisabledActions,
    'slide.crm_product__v.id': crmProductId,
    'slide.crm_ios_viewer__v': 'WKWebView',
    'slide.ios_resolution__v': 'Scale To Fit'
  },
  shared: {
    'slide.product__v.name__v': '',
    'slide.country__v.name__v': country,
    'name__v': id + '-Shared'
  }
};

var msccvProduction = {
  presentation: {
    'pres.crm_presentation_id__v': id,
    'external_id__v': id,
    'pres.product__v.name__v': '',
    'pres.country__v.name__v': country,
    'pres.language__v': language,
    'pres.crm_hidden__v': presHidden,
  },
  slide: {
    'slide.product__v.name__v': '',
    'slide.language__v': language,
    'slide.country__v.name__v': country,
    'slide.crm_disable_actions__v': slideDisabledActions,
    'slide.crm_ios_viewer__v': 'WKWebView',
    'slide.ios_resolution__v': 'Scale To Fit'
  },
  shared: {
    'slide.product__v.name__v': '',
    'slide.country__v.name__v': country,
    'name__v': id + '-Shared'
  }
}

// Set `msccvProduction` for production vault or `msccvStage` for RMH vault
var mccsv = msccvStage;

module.exports = {
  'id': id,
  'name': name,
  'platform': 'veeva-v1',
  'gruntgenVersion': '2',
  'extIdSuffix': '_extId',
  'debug': { 'hostname': 'localhost' },
  'widgets': ['modal', 'video', 'veemail-collect', 'veemail-send'],
  'packs': ['data-tap-helper', 'simple-slide-navigation', 'storage'],
  'cli': {
    babel: true,
    frip: false
  },
  'layers': [],
  'platformSettings': {
    useVaultPackagingFormat: true,
    uploadType: 'mccsv',
    mccsv: mccsv,
    vaultDomain: 'https://rmh-contentpartners-mc2.veevavault.com',
    sharedResource: {
      zipname: id + '-Shared.zip'
    }
  }
};
